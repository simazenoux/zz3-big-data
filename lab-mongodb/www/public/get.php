<?php

include_once '../init.php';

use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

$twig = getTwig();
$manager = getMongoDbManager();

// @todo implementez la récupération des données d'une entité et la passer au template
// petite aide : https://github.com/VSG24/mongodb-php-examples

$_id = $_GET["_id"];

if ($_ENV["REDIS_ENABLE"]){
    $redis_client = getRedisClient();

    $book = json_decode($redis_client->get("book{$_id}"), true);
    if (!$book){
        $book = $manager->selectCollection('books')->findOne(['_id' => new MongoDB\BSON\ObjectId($_id)]);
        $redis_client->set("book{$_id}", json_encode($book));
    }
}
else{
    $book = $manager->selectCollection('books')->findOne(['_id' => new MongoDB\BSON\ObjectId($_id)]);
}


// render template
try {
    echo $twig->render('get.html.twig', ['entity' => $book]);
} catch (LoaderError|RuntimeError|SyntaxError $e) {
    echo $e->getMessage();
}   
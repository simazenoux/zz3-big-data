<?php

include_once '../init.php';

use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

$twig = getTwig();
$manager = getMongoDbManager();

$books_per_page = 20;

$total_number_of_books = $manager->selectCollection('books')->count();

$total_number_of_pages = (int) ($total_number_of_books / $books_per_page);



if (isset($_GET['page'])){
    $page = $_GET['page'];
    if ($page > $total_number_of_pages) {
        $page = $total_number_of_pages;
    }
}
else{
    $page = 1;
}

$previous_page = $page-1;
if ($page == 1){
    $previous_page = 1;
}
$url_previous_page = "/?page=$previous_page";

$next_page = $page + 1;
if ($page == $total_number_of_pages){
    $next_page = $total_number_of_pages;
}
$url_next_page = "/?page=$next_page";


$skip =  (int) ($books_per_page * ($page - 1));

// render template
try {
    if ($_ENV["REDIS_ENABLE"]){
        $redis_client = getRedisClient();
        $render = $redis_client->get("page{$page}");
        if (!$render){
            $books = $manager->selectCollection('books')->find([], ['limit' => $books_per_page, 'skip' => $skip]);
            $render = $twig->render('index.html.twig', ['list' => $books, 'page'=>$page, 'url_previous_page' => $url_previous_page, 'url_next_page' => $url_next_page]);
            $redis_client->set("page{$page}", $render);
        }
    }
    else{
        $books = $manager->selectCollection('books')->find([], ['limit' => $books_per_page, 'skip' => $skip]);
        $render = $twig->render('index.html.twig', ['list' => $books, 'page'=>$page, 'url_previous_page' => $url_previous_page, 'url_next_page' => $url_next_page]);
    }
    echo $render;
} 
catch (LoaderError|RuntimeError|SyntaxError $e) {
    echo $e->getMessage();
}


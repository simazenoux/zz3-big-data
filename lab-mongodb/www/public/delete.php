<?php

include_once '../init.php';

$manager = getMongoDbManager();
$redis_client = getRedisClient();

$_id = $_GET["_id"];

$book = $manager->selectCollection('books')->deleteOne(['_id' => new MongoDB\BSON\ObjectId($_id)]);
$redis_client->del("book{$_id}");
$redis_client->del($redis_client->keys('page*'));
header('Location: /');
<?php

include_once '../init.php';

use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

$twig = getTwig();
$manager = getMongoDbManager();

if (!empty($_POST)) {
    $book = [
        'title' => $_POST['title'],
        'author' => $_POST['author'],
        'century' => $_POST['century'],
    ];
    $manager->selectCollection('books')->updateOne(
        ['_id' => new MongoDB\BSON\ObjectId($_POST["id"])],
        ['$set' => ['title' => $_POST['title'], 'author' => $_POST['author'], 'century' => $_POST['century']]]
    );
    $redis_client = getRedisClient();
    $redis_client->set("book{$_id}", json_encode($book));
    $redis_client->del($redis_client->keys('page*'));
    header('Location: /');
} 
else {
    $book = $manager->selectCollection('books')->findOne(['_id' => new MongoDB\BSON\ObjectId($_GET["_id"])]);
    try {
        echo $twig->render('update.html.twig', ['entity' => $book]);
    } catch (LoaderError|RuntimeError|SyntaxError $e) {
        echo $e->getMessage();
    }
}
# Lab 

## Technology used:

PHP, Mongodb, Twig, Docker, Docker-Compose

## 1 - Lab MongoDB

1. Install Docker on your machine using the Docker documentation

2. Clone the following repo : https://github.com/citizen63000/tp-mongodb

3. Import the list of manuscripts from the library of Clermont-Ferrand library into your local MongoDB database. You can find the list of manuscripts [here](https://opendata.clermontmetropole.eu/explore/dataset/catalogue-manuscrits-bibliotheque-patrimoine/information/)

4. Please implement: 
    - the addition of an element (create.php and associated template)
    - a page for viewing an element (file get.php and associated template)
    - the deletion of an entry
    - the modification of a line (edit.php and appropriate template)

5. Bonus:
    - Implement pagination

## 2 - Lab Redis

6. Add redis and redis-commander to the docker-compose.yml file.

## 2 - Quickstart

To launch the server:
``` console
docker-compose up
```

To access the website : http://localhost:8080

To access mongo-express: http://localhost:8081

To access redis-commander: http://localhost:8082

To access grafana: http://localhost:3000



